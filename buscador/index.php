<html>
<head>
	<meta charset="UTF-8">
	<title>Fiestas del Pilar 2015 | Buscador</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">

	<link rel="stylesheet" href="../css/main.css">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../css/bootstrap-datetimepicker.css">

	<link rel="icon" type="image/png" href="../img/favicon.png">
</head>
<body>

	<!--*******************************************************************************NAVBAR***********************************************************************************************-->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href=".."><img style="max-width:90px; margin-top: -37px;" alt="Brand" src="../img/favicon.png"></a>
				<a class="navbar-brand" href="..">Pilares 2015</a>
			</div>

			<!--<a class="navbar-brand" href="#">Pilares 2015</a>-->
		
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				
				<ul class="nav navbar-nav text-center">
					<li><a href="../buscador?srcname=">Eventos</a></li>
				</ul>

				<ul class="nav navbar-nav text-center">
					<li><a href= <?php echo '"../buscador?fechainicio=' . str_replace("/", "%2F", date("d/m/Y")) . '"' ?>>Hoy</a></li>
					<!--02%2F10%2F2015-->
				</ul>

				<ul class="nav navbar-nav text-center">
					<?php 
					$datemanana;
					if(date("d") > date("t")){
						$datedia = 1;
						$datemes = date("m") + 1;
						$datemanana = str_replace("/", "%2F", $datedia . "/" . $datemes . date("/Y"));
					}else{
						$datedia = date("d") + 1;
						$datemanana = str_replace("/", "%2F", $datedia . "/" . date("m/Y"));
					}

					 ?>
					<li><a href= <?php echo '"../buscador?fechainicio=' . $datemanana . '"' ?>>Mañana</a></li>
				</ul>

			
				<form class="nav navbar-form navbar-right visible-xs" role="search" action="../buscador">
						<div class="row">
							<div class="form-group">
								<div class="col-xs-1"></div>
								<div class="col-xs-8">
									<input type="text" class="form-control" name="srcname" placeholder="Búsqueda Rápida">
								</div>
								<div class="col-xs-3">
									<button type="submit" class="btn btn-default">Buscar</button>
								</div>
							</div>
						</div>
				</form>

				<form class="nav navbar-form navbar-right hidden-xs hidden-sm" role="search" action="../buscador">
					<div class="form-group">
						<input type="text" class="form-control hidden-xs" name="srcname" placeholder="Búsqueda Rápida">
						<button type="submit" class="btn btn-default hidden-xs">Buscar</button>
					</div>
				</form>
				
				<form class="nav navbar-form navbar-right visible-sm" role="search" action="../buscador">
					<div class="form-group">
						<input type="text" style="width: 150px;" class="form-control hidden-xs" name="srcname" placeholder="Búsqueda Rápida">
						<button type="submit" class="btn btn-default hidden-xs">Buscar</button>
					</div>
				</form>
			</div>
		</div>
	</nav>

	<?php 

	$srchPAGE = '';

	if(empty($_GET['page']) || $_GET['page'] == 0){
		$srchPAGE = '&start=0';
	}else{
		$start = $_GET['page'] * 20;
		$srchPAGE = '&start=' . $start;
	}

	$srchTEMA = '';
	$srchSTR = '';

	if(!empty($_GET['tema']) && $_GET['tema'] != "0"){
		$srchTEMA = ';temas.id==' . $_GET['tema'];
	}

	if(!empty($_GET['srcname'])){
		$srchSTR = ';title==*' . str_replace(" ", "%20", strtolower($_GET['srcname'])) . '*';
	}

	$srchFECHA = '';

	//Date: 2015-10-09T00:00:00Z

	if(!empty($_GET['fechainicio'])){
		if (!empty($_GET['fechafin'])) {
			//Fecha inicio 	LLENO
			//Fecha fin 	LLENO
			$datearray = explode('/', $_GET['fechainicio']);
			$fechainicio = $datearray[2] . "-" . $datearray[1] . "-" . $datearray[0] . "T00:00:00Z";

			$datearray2 = explode('/', $_GET['fechafin']);
			$fechafin = $datearray2[2] . "-" . $datearray2[1] . "-" . $datearray2[0] . "T00:00:00Z";
			//echo $fechainicio . "<br>" . $fechafin;
			$srchFECHA = ";startDate=ge=" . $fechainicio . ";endDate=le=" . $fechafin . "T00:00:00Z";
			//startDate=ge=2015-03-01T00:00:00Z;endDate=le=2015-03-31T00:00:00Z
		}else{
			//Fecha inicio 	LLENO
			//Fecha fin 	VACIO

			//;startDate=le=FECHA;endDate=ge=FECHA
			$datearray = explode('/', $_GET['fechainicio']);
			$fechainicio = $datearray[2] . "-" . $datearray[1] . "-" . $datearray[0] . "T00:00:00Z";

			//echo $fechainicio; 

			$srchFECHA = ";startDate=le=" . $fechainicio . ";endDate=ge=" . $fechainicio . "T00:00:00Z";
		}
		
	}elseif(!empty($_GET['fechafin'])){
		if (!empty($_GET['fechainicio'])) {
			//Fecha fin 	LLENO
			//Fecha inicio 	LLENO
			//SOBRA
			$datearray = explode('/', $_GET['fechainicio']);
			$fechainicio = $datearray[2] . "-" . $datearray[1] . "-" . $datearray[0] . "T00:00:00Z";

			$datearray2 = explode('/', $_GET['fechafin']);
			$fechafin = $datearray2[2] . "-" . $datearray2[1] . "-" . $datearray2[0] . "T00:00:00Z";
			//echo $fechainicio . "<br>" . $fechafin;
		}else{
			//Fecha fin 	LLENO
			//Fecha inicio 	VACIO

			$datearray = explode('/', $_GET['fechafin']);
			$fechafin = $datearray[2] . "-" . $datearray[1] . "-" . $datearray[0] . "T00:00:00Z";

			//echo $fechafin; 

			$srchFECHA = ";startDate=le=" . $fechafin . ";endDate=ge=" . $fechafin . "T00:00:00Z";
		}
		
	}else{
		//Fecha fin 	VACIO
		//Fecha inicio 	VACIO
	}
	//echo $srchPAGE;
	$weblist = 'http://zaragoza.es/api/recurso/cultura-ocio/evento-zaragoza.json?fl=id,title,startDate,endDate,image,temas,subEvent&rows=20' . $srchPAGE . '&q=programa==Fiestas%20del%20Pilar' . $srchTEMA . $srchSTR . $srchFECHA . '&sort=startDate%20asc&sort=endDate%20asc';

	//echo $weblist . "<br>";

	$weblistcontent = file_get_contents($weblist);
	$weblistjson = json_decode($weblistcontent, true); 
	//echo $weblistcontent;
	
	$srchtotalCount = '';

	if($weblistjson['totalCount'] != "0"){
		$listresult = $weblistjson['result'];
		$srchtotalCount = $weblistjson['totalCount'];
	}


	?>
	<div class="container">
		<div class="main row first_container_not_right">
			<div class="container col-xs-12 col-sm-3 col-md-3">

				<div class="form-group">
					<form action="">

						<label>Buscar</label>
						<input type="text" class="form-control" name="srcname" placeholder="Ofrend...">

						<br>
						<label>Tema</label>
						<select class="form-control" name="tema">
						<option value="0">Todos</option>
						<option value="79">Actos Religiosos</option>
						<option value="37">Bibliotecas</option>
						<option value="25">Deportes</option>
						<option value="19">Exposiciones y Museos</option>
						<option value="23">Ferias, Muestras y Salones</option>
						<option value="22">Fiestas</option>
						<option value="16">Música</option>
						<option value="26">Otros</option>
						<option value="17">Teatro</option>
						</select>
						<br>
						<label>Fecha</label>
						<div class='input-group date' id='datetimepicker6'>
							<input type='text' class="form-control" name="fechainicio" placeholder="Fecha Inicio"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
						<br>
						<div class='input-group date' id='datetimepicker7'>
							<input type='text' class="form-control" name="fechafin" placeholder="Fecha Final (Opcional)"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
						<br>
						<input class="btn btn-default" type="submit" value="Buscar">
					</form>

				</div>

			</div>
			<?php 
			if($weblistjson['totalCount'] == "0"){
				?>
				<div class="container col-xs-12 col-sm-12 col-md-9 text-center">
					<h2>Ops!<small> Parece que no hay nada por aquí</small></h2>
					<h3><small>Intenta buscar por título o haz búsquedas más genéricas.</small></h3>
				</div>
				<?php				
			}else{ ?>
			<div class="container col-xs-12 col-sm-9 col-md-9">
				<table class="table">
					<tr data-href='#'>
						<td></td>
						<td class='hidden-xs'><strong>Fecha</strong></td>
						<td class="visible-xs"><strong>Fecha</strong></td>
						<td><strong>Evento</strong></td>
						<td><strong>Hora</strong></td>
					</tr>
					<?php 
					foreach ($listresult as $key){
						echo "<tr class='clickable-row' data-href='../evento/?id=" . $key['id'] . "'>";
						if (!empty($key['image'])) {
							//echo '<td><img class="img-circle img-responsive" height="35px" width="35px" src=' . str_replace(" ", "%20", $key['image']) . '></td>';
							echo "<td><div class='image-cropper'><img src='" . str_replace(" ", "%20", $key['image']) . "' class='rounded' height='30' width='30'/></div></td>";
						}else{
							//echo '<td><img class="img-circle img-responsive" height="35px" width="35px"  src="http://zaragoza.es/cont/paginas/actividades/' . $key['temas'][0]['image'] .'"></td>';
							echo "<td><div class='image-cropper'><img src='../img/temas/" . $key['temas'][0]['id'] . "_opt.jpg' class='rounded' height='30' width='30'/></div></td>";
						}

						if (!empty($key['startDate'])) {
							$fechainicio = explode("-", substr($key['startDate'], 0, 10)); 
							echo "<td class='hidden-xs'>" . $fechainicio[2] . '/' . $fechainicio[1] . '/' . $fechainicio[0] . "</td>";
						}else{
							$fechafin = explode("-", substr($key['endDate'], 0, 10)); 
							echo "<td class='hidden-xs'>" . $fechafin[2] . '/' . $fechafin[1] . '/' . $fechafin[0] . "</td>";
						}

						if (!empty($key['startDate'])) {
							$fechainicio = explode("-", substr($key['startDate'], 0, 10)); 
							echo "<td class='visible-xs'>" . $fechainicio[2] . '/' . $fechainicio[1] . '/' . str_replace("20", "", $fechainicio[0]) . "</td>";
						}else{
							$fechafin = explode("-", substr($key['endDate'], 0, 10)); 
							echo "<td class='visible-xs'>" . $fechafin[2] . '/' . $fechafin[1] . '/' . str_replace("20", "", $fechainicio[0]) . "</td>";
						}

						echo "<td>" . $key['title'] . "</td>";
						if(!empty($key['subEvent'][0]['horaInicio'])){
							echo "<td>" . $key['subEvent'][0]['horaInicio'] . "</td>";
						}else{
							echo "<td>   ---</td>";
						}
						echo "</tr>";

					}

					?>


				</table>
			</div>
			<?php } ?>

		</div>

		<div class="row nav_not_right">
			<div class="hidden-xs col-sm-3 col-md-3"></div>
			<div class="col-xs-12 col-sm-9 col-md-9 text-center">
				<nav>
					<ul class="pager">
						<?php 
						if(empty($_GET['page']) || $_GET['page'] == 0){?>
							<li class="disabled"><a>Anterior</a></li><?php 
						}else{
							$actual_link = str_replace("&page=" . $_GET['page'], "", "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
							$lastpage = $_GET['page'] - 1;
							?><li><a href=<?php echo '"' . $actual_link . '&page=' . $lastpage . '"'; ?> >Anterior</a></li><?php
							
							} ?>
					
							<?php 	
							if(!empty($_GET['page'])){
								if ((($_GET['page'] + 1) * 20) >= $srchtotalCount ) {
									?><li class="disabled"><a>Siguiente</a></li><?php
								}else{
									$actual_link = str_replace("&page=" . $_GET['page'], "", "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
									$nextpage = $_GET['page'] + 1;
									?><li><a href=<?php echo '"' . $actual_link . '&page=' . $nextpage . '"'; ?> >Siguiente</a></li><?php
								}
							}else{
								if (((0) +1 * 20) >= $srchtotalCount ) {
									?><li class="disabled"><a>Siguiente</a></li><?php
								}else{
									if (empty($_GET['page'])) {
										if(isset($_GET['page'])){
											$actual_link = str_replace("&page=" . $_GET['page'], "", "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
										}else{
											$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
										}
									}else{
										$actual_link = str_replace("&page=" . $_GET['page'], "", "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
									}

									
									?><li><a href=<?php echo '"' . $actual_link . '&page=1' . '"'; ?> >Siguiente</a></li><?php
								}
							}
						?>

					</ul>
				</nav>
			</div>
		</div>
	</div>

	<footer class="footer footer_not_right">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-6 text_not_right hidden-xs">
					 	<?php 
						$web_nactos = 'http://www.zaragoza.es/api/recurso/cultura-ocio/evento-zaragoza.json?fl=id&q=programa==Fiestas%20del%20Pilar&rows=1';
						$web_nactoscontent = file_get_contents($web_nactos);
						$web_nactosjson = json_decode($web_nactoscontent, true); 
						?>

						<p class="hidden-xs text-muted text-left">Hay <strong><?php echo $web_nactosjson['totalCount']; ?></strong> eventos en las Fiestas del Pilar</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 text_not_right">
					<p class="text-muted text-right">Origen de los datos: Ayuntamiento de Zaragoza</p>
				</div>
			</div>
		</div>
	</footer>

	<script src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/moment.js"></script>
	<script src="../js/bootstrap-datetimepicker.js"></script>
	<script src="../js/es.js"></script>

	<script>

	$(document).ready(function(){
	    $('table tr').click(function(){
	        window.location = $(this).data('href');
	        return false;
	    });
	});
	  
	</script>

							<script type="text/javascript">
						    $(function () {
						        $('#datetimepicker6').datetimepicker({
						        format: 'DD/MM/YYYY',

						        minDate: new Date(2015, 10 - 1, 1),
						          maxDate: new Date(2015, 10 - 1, 31),
						        locale: 'es',
						        useCurrent: false,
						        showClear: true,
						        showClose: true
						      });
						        $('#datetimepicker7').datetimepicker({
						          format: 'DD/MM/YYYY',

						          minDate: new Date(2015, 10 - 1, 1),
						          maxDate: new Date(2015, 10 - 1, 31),
						          locale: 'es',
						            useCurrent: false,
						            showClear: true,
						            showClose: true
						        });
						        $("#datetimepicker6").on("dp.change", function (e) {
						            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
						        });
						        $("#datetimepicker7").on("dp.change", function (e) {
						            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
						        });
						    });
						</script>
						<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=10640487; 
var sc_invisible=1; 
var sc_security="102d0788"; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="shopify stats"
href="http://statcounter.com/shopify/" target="_blank"><img
class="statcounter"
src="http://c.statcounter.com/10640487/0/102d0788/1/"
alt="shopify stats"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->
</body>
</html>