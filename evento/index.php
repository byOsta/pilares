<html>
<head>
	<meta charset="UTF-8">
	
	<?php 
		if (empty($_GET['id'])) {
			echo "<title>Fiestas del Pilar | Error 404</title>";
		}else{
			$web_actoid = 'http://www.zaragoza.es/api/recurso/cultura-ocio/evento-zaragoza/'.$_GET['id'].'.json';
			function get_http_response_code($url) {
			$headers = get_headers($url);
			return substr($headers[0], 9, 3);
		}
			if(get_http_response_code($web_actoid) != "200"){
				echo "<title>Fiestas del Pilar | Error 404</title>";
			}else{
				$web_actoidcontent = file_get_contents($web_actoid);
				$web_actoidjson = json_decode($web_actoidcontent, true);
				echo "<title>Fiestas del Pilar | ".$web_actoidjson['title']."</title>";
			}
		}
	?>

	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../css/main.css">

	<link rel="icon" type="image/png" href="../img/favicon.png">

</head>
<body>

	<!--*******************************************************************************NAVBAR***********************************************************************************************-->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href=".."><img style="max-width:90px; margin-top: -37px;" alt="Brand" src="../img/favicon.png"></a>
				<a class="navbar-brand" href="..">Pilares 2015</a>
			</div>

			<!--<a class="navbar-brand" href="#">Pilares 2015</a>-->
		
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				
				<ul class="nav navbar-nav text-center">
					<li><a href="../buscador?srcname=">Eventos</a></li>
				</ul>

				<ul class="nav navbar-nav text-center">
					<li><a href= <?php echo '"../buscador?fechainicio=' . str_replace("/", "%2F", date("d/m/Y")) . '"' ?>>Hoy</a></li>
					<!--02%2F10%2F2015-->
				</ul>

				<ul class="nav navbar-nav text-center">
					<?php 
					$datemanana;
					if(date("d") > date("t")){
						$datedia = 1;
						$datemes = date("m") + 1;
						$datemanana = str_replace("/", "%2F", $datedia . "/" . $datemes . date("/Y"));
					}else{
						$datedia = date("d") + 1;
						$datemanana = str_replace("/", "%2F", $datedia . "/" . date("m/Y"));
					}

					 ?>
					<li><a href= <?php echo '"../buscador?fechainicio=' . $datemanana . '"' ?>>Mañana</a></li>
				</ul>

			
				<form class="nav navbar-form navbar-right visible-xs" role="search" action="../buscador">
						<div class="row">
							<div class="form-group">
								<div class="col-xs-1"></div>
								<div class="col-xs-8">
									<input type="text" class="form-control" name="srcname" placeholder="Búsqueda Rápida">
								</div>
								<div class="col-xs-3">
									<button type="submit" class="btn btn-default">Buscar</button>
								</div>
							</div>
						</div>
				</form>

				<form class="nav navbar-form navbar-right hidden-xs hidden-sm" role="search" action="../buscador">
					<div class="form-group">
						<input type="text" class="form-control hidden-xs" name="srcname" placeholder="Búsqueda Rápida">
						<button type="submit" class="btn btn-default hidden-xs">Buscar</button>
					</div>
				</form>
				
				<form class="nav navbar-form navbar-right visible-sm" role="search" action="../buscador">
					<div class="form-group">
						<input type="text" style="width: 150px;" class="form-control hidden-xs" name="srcname" placeholder="Búsqueda Rápida">
						<button type="submit" class="btn btn-default hidden-xs">Buscar</button>
					</div>
				</form>
			</div>
		</div>
	</nav>

	<div class="container">		
			<?php 
				if (empty($_GET['id'])) {
				?>
					<div class="main row first_container_not_right">
						<div class="col-xs-12 col-sm-12 col-md-12 text-center">
							<h2>Error 404 <small>Not Found</small></h2>
						</div>
					</div>
				<?php 
				}else{
					$web_actoid = 'http://www.zaragoza.es/api/recurso/cultura-ocio/evento-zaragoza/'.$_GET['id'].'.json?srsname=wgs84';

					if(get_http_response_code($web_actoid) != "200"){
					?>
						<div class="main row first_container_not_right">
							<div class="col-xs-12 col-sm-12 col-md-12 text-center">
								<h2>Error 404 <small>Not Found</small></h2>
							</div>
						</div>
					<?php 
					}else{

						/********************************************************************JSON**************************************************************************************/

						$web_actoidcontent = file_get_contents($web_actoid);
						$web_actoidjson = json_decode($web_actoidcontent, true);

						/********************************************************************IMAGE**************************************************************************************/
						$eventimagedata;
						$eventimage;

						if (array_key_exists("image", $web_actoidjson)) {
							$eventimage = '<img class="img-responsive" src=' . str_replace(" ", "%20", $web_actoidjson['image']) . '>';
							$eventimagedata = str_replace(" ", "%20", $web_actoidjson['image']);
						}else{
							$eventimage = '<img class="img-responsive" src="http://zaragoza.es/cont/paginas/actividades/' . $web_actoidjson['temas'][0]['image'] .'">';
							$eventimagedata = '"http://zaragoza.es/cont/paginas/actividades/' . $web_actoidjson['temas'][0]['image'] . '"';
						}

						/********************************************************************TITLE**************************************************************************************/

						$eventtitle;

						if(array_key_exists("web", $web_actoidjson)){
							$eventtitle = '<a href="'. $web_actoidjson['web'] . '" target="_blank"><h3>'.$web_actoidjson['title'].'</h3></a>';
						}else{
							$eventtitle = '<h3>'.$web_actoidjson['title'].'</h3>';
						}

						/********************************************************************ANEXODOC**************************************************************************************/

						$eventanexodoc;
						$eventanexodocbool = false;
						if(array_key_exists("anexo", $web_actoidjson)){
							if(array_key_exists("documento", $web_actoidjson['anexo'][0])){
								$eventanexodoc = $web_actoidjson['anexo'][0]['documento'];
								$eventanexodocbool = true;
							}
						}
						

						/********************************************************************TEMAS**************************************************************************************/

						$eventtema;
						$eventtemabool = false;
						if(array_key_exists("temas", $web_actoidjson)){
							$eventtema = $web_actoidjson['temas'][0]['title'];
							$eventtemabool = true;
						}

						/********************************************************************LAST UPDATE**************************************************************************************/

						$eventupdate;
						$eventupdatebool = false;
						if(array_key_exists("lastUpdated", $web_actoidjson)){
							$eventupdate = $web_actoidjson['lastUpdated'];
							$eventupdatebool = true;
						}


						/********************************************************************DESCRIPTION**************************************************************************************/

						$eventdescbool = false;
						$eventdesc;

						if(array_key_exists("description", $web_actoidjson)){
							$eventdescbool = true;
							$eventdesc = $web_actoidjson['description'];
						}

						/********************************************************************POBLACIÓN**************************************************************************************/

						$eventpoblacionbool = false;
						$eventpoblacion;

						if(array_key_exists("poblacion", $web_actoidjson)){
							$eventpoblacionbool = true;
							$eventpoblacion = $web_actoidjson['poblacion'][0]['title'];
						}

						/********************************************************************GEOMETRY**************************************************************************************/

						$eventgeometrybool = false;
						$eventgeometry;

						if(array_key_exists("geometry", $web_actoidjson)){
							$eventgeometrybool = true;
							$eventgeometry = $web_actoidjson['geometry'];
						}

						/********************************************************************HORARIO INSCRIPCIÓN**************************************************************************************/

						$eventhorinscbool = false;
						$eventhorinsc;

						if(array_key_exists("lugarInscripcion", $web_actoidjson)){
							$eventhorinscbool = true;
							$eventhorinsc = $web_actoidjson['lugarInscripcion'];
						}
						
						/*************************************************************************subEvent**************************************************************************************/

						$eventsubEventbool = false;
						$eventsubEvent;

						$eventhorariobool = false;
						$eventhorario;

						$eventhorainiciobool = false;
						$eventhorainicio;
						$eventhorafinalbool =  false;
						$eventhorafinal;

						$eventlugarbool = false;
						$eventlugar;

						$eventfechainiciobool = false;
						$eventfechainicio;
						$eventfechafinalbool = false;
						$eventfechafinal;

						if(array_key_exists("subEvent", $web_actoidjson)){
							$eventsubEventbool = true;
							$eventsubEvent = $web_actoidjson['subEvent'][0];
							
							if(array_key_exists("horario", $eventsubEvent)){
								$eventhorariobool = true;
								$eventhorario = $eventsubEvent['horario'];
							}
							if (array_key_exists("horaInicio", $eventsubEvent)){
								$eventhorainicio = $eventsubEvent['horaInicio'];
								$eventhorainiciobool = true;
							}
							if (array_key_exists("horaFinal", $eventsubEvent)){
								$eventhorafinal = $eventsubEvent['horaFinal'];
								$eventhorafinalbool = true;
							}

							if(array_key_exists("lugar", $eventsubEvent)){
								$eventlugar = $eventsubEvent['lugar'];
								$eventlugarbool = true;
							}

							if(array_key_exists("fechaInicio", $eventsubEvent)){
								$eventfechainicio = $eventsubEvent['fechaInicio'];
								$eventfechainiciobool = true;
							}
							if(array_key_exists("fechaFinal", $eventsubEvent)){
								$eventfechafinal = $eventsubEvent['fechaFinal'];
								$eventfechafinalbool = true;
							}

							if($eventgeometrybool == false && array_key_exists("geometry", $eventsubEvent)){
								$eventgeometry =  $eventsubEvent['geometry'];
							}
						}else{
							$eventsubEventbool = false;
						}

						/********************************************************************LUGAR**************************************************************************************/
						$eventlugartitlebool = false;
						$eventlugardireccionbool = false;
						$eventlugarcpbool = false;
						$eventlugarlocalidadbool = false;
						$eventlugarprovinciabool = false;
						$eventlugarpaisbool = false;
						$eventlugarmailbool = false;
						$eventlugaraccesibilidadbool = false;
						$eventlugarautobusesbool = false;
						$eventlugarfaxbool = false;
						$eventlugartelefonobool = false;
						if($eventlugarbool == true){
							if(array_key_exists("title", $eventlugar)){
								$eventlugartitle = $eventlugar['title'];
								$eventlugartitlebool = true;
							}
							if(array_key_exists("direccion", $eventlugar)){
								$eventlugardireccion = $eventlugar['direccion'];
								$eventlugardireccionbool = true;
							}
							if(array_key_exists("cp", $eventlugar)){
								$eventlugarcp = $eventlugar['cp'];
								$eventlugarcpbool = true;
							}
							if(array_key_exists("localidad", $eventlugar)){
								$eventlugarlocalidad = $eventlugar['localidad'];
								$eventlugarlocalidadbool = true;
							}
							if(array_key_exists("provincia", $eventlugar)){
								$eventlugarprovincia = $eventlugar['provincia'];
								$eventlugarprovinciabool = true;
							}
							if(array_key_exists("pais", $eventlugar)){
								$eventlugarpais = $eventlugar['pais'];
								$eventlugarpaisbool = true;
							}
							if(array_key_exists("mail", $eventlugar)){
								$eventlugarmail = $eventlugar['mail'];
								$eventlugarmailbool = true;
							}
							if(array_key_exists("accesibilidad", $eventlugar)){
								$eventlugaraccesibilidad = $eventlugar['accesibilidad'];
								$eventlugaraccesibilidadbool = true;
							}
							if(array_key_exists("autobuses", $eventlugar)){
								$eventlugarautobuses = $eventlugar['autobuses'];
								$eventlugarautobusesbool = true;
							}
							if(array_key_exists("fax", $eventlugar)){
								$eventlugarfax = $eventlugar['fax'];
								$eventlugarfaxbool = true;
							}
							if(array_key_exists("telefono", $eventlugar)){
								$eventlugartelefono = $eventlugar['telefono'];
								$eventlugartelefonobool = true;
							}
						}

						/********************************************************************ENTIDAD************************************************************************************/
						$evententidadbool = false;
						
						$evententidadtitlebool = false;
						$evententidaddireccionbool = false;
						$evententidadcpbool = false;
						$evententidadlocalidadbool = false;
						$evententidadprovinciabool = false;
						$evententidadpaisbool = false;
						$evententidadtelefonobool = false;
						$evententidadtelefono2bool = false;
						$evententidadfaxbool = false;
						$evententidademailbool = false;
						$evententidadhorariobool = false;
						$evententidadwebbool = false;

						if(array_key_exists("entidad", $web_actoidjson)){
							if(array_key_exists("title", $web_actoidjson['entidad'])){
								$evententidadbool = true;
								$evententidad = $web_actoidjson['entidad'];

								if(array_key_exists("title", $evententidad)){
									$evententidadtitle = $evententidad['title'];
									$evententidadtitlebool = true;
								}
								if(array_key_exists("direccionEntidad", $evententidad)){
									$evententidaddireccion = $evententidad['direccionEntidad'];
									$evententidaddireccionbool = true;
								}
								if(array_key_exists("cpEntidad", $evententidad)){
									$evententidadcp = $evententidad['cpEntidad'];
									$evententidadcpbool = true;
								}
								if(array_key_exists("localidadEntidad", $evententidad)){
									$evententidadlocalidad = $evententidad['localidadEntidad'];
									$evententidadlocalidadbool = true;
								}
								if(array_key_exists("provinciaEntidad", $evententidad)){
									$evententidadprovincia = $evententidad['provinciaEntidad'];
									$evententidadprovinciabool = true;
								}
								if(array_key_exists("paisEntidad", $evententidad)){
									$evententidadpais = $evententidad['paisEntidad'];
									$evententidadpaisbool = true;
								}
								if(array_key_exists("telefonoEntidad", $evententidad)){
									$evententidadtelefono = $evententidad['telefonoEntidad'];
									$evententidadtelefonobool = true;
								}
								if(array_key_exists("telefono2Entidad", $evententidad)){
									$evententidadtelefono2 = $evententidad['telefono2Entidad'];
									$evententidadtelefono2bool = true;
								}
								if(array_key_exists("faxEntidad", $evententidad)){
									$evententidadfax = $evententidad['faxEntidad'];
									$evententidadfaxbool = true;
								}
								if(array_key_exists("emailEntidad", $evententidad)){
									$evententidademail = $evententidad['emailEntidad'];
									$evententidademailbool = true;
								}
								if(array_key_exists("horarioEntidad", $evententidad)){
									$evententidadhorario = $evententidad['horarioEntidad'];
									$evententidadhorariobool = true;
								}
								if(array_key_exists("webEntidad", $evententidad)){
									$evententidadweb = $evententidad['webEntidad'];
									$evententidadwebbool = true;
								}
							}
						}

						/********************************************************************OTROS DATOS***************************************************************************/

						$eventfestividadlocal = false;
						$eventbarrerasarquit = false;
						$eventprepdiscapacidad = false;
						$evententradalibre = false;

						$eventprecioentradabool = false;

						if(array_key_exists("festividadLocal", $web_actoidjson)){
							$eventfestividadlocal = true;
						}
						if(array_key_exists("barrerasArquit", $web_actoidjson)){
							$eventbarrerasarquit = true;
						}
						if(array_key_exists("prepDiscapacidad", $web_actoidjson)){
							$eventprepdiscapacidad = true;
						}
						if(array_key_exists("tipoEntrada", $web_actoidjson)){
							$evententradalibre = true;
						}


						if(array_key_exists("precioEntrada", $web_actoidjson)){
							$eventprecioentrada = $web_actoidjson['precioEntrada'];
							$eventprecioentradabool = true;
						}

						/********************************************************************START & END DATE***************************************************************************/

						if($eventfechainiciobool == false){
							if(array_key_exists("startDate", $web_actoidjson)){
								$eventfechainiciobool = true;
								$eventfechainicio = $web_actoidjson['startDate'];
							}else{
								$eventfechainiciobool = false;
							}
						}

						if($eventfechafinalbool == false){
							if(array_key_exists("endDate", $web_actoidjson)){
								$eventfechafinalbool = true;
								$eventfechafinal = $web_actoidjson['endDate'];
							}else{
								$eventfechafinalbool = false;
							}
						}
						
						?>
						<div class="container row first_container_not_right">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-3 text-center">
									<div class="visible-xs ">
										<center>
										<table>
											<tr>
												<td><div class='image-cropper'><img src= <?php echo $eventimagedata ?> class='rounded' /></div> </td>
												<td><p>    </p></td>
												<td><?php echo $eventtitle; ?></td>
											</tr>
										</table> 
										</center>
									</div>
									
									<div class="hidden-xs"> 
										<?php echo $eventtitle; ?>
									</div>									
								</div>
								<div class=" col-xs-12 col-sm-12 col-md-9 text-justify">
									<div class="hidden-xs">
										<ul class="tabpanespace nav nav-tabs nav-justified">
											
											<li class="active"><a data-toggle="tab" href="#description">Descripción</a></li> <?php
											
											if ($eventsubEventbool) {
												?><li><a data-toggle="tab" href="#subEvent">Como llegar</a></li><?php
											}
											if ($evententidadbool) {
												?><li><a data-toggle="tab" href="#entidad">Entidad</a></li><?php
											}
											?>
											
											<li><a data-toggle="tab" href="#compartir">Compartir </a></li>
										</ul>
									</div>
									<div class="visible-xs">
										<ul class="nav nav-tabs nav-justified">
											
											<li class="active"><a data-toggle="tab" href="#description">Descripción</a></li> <?php
											
											if ($eventsubEventbool) {
												?><li><a data-toggle="tab" href="#subEvent">Como llegar</a></li><?php
											}
											if ($evententidadbool) {
												?><li><a data-toggle="tab" href="#entidad">Entidad</a></li><?php
											}
											?>
											
											<li><a data-toggle="tab" href="#compartir">Compartir </a></li>
										</ul>
									</div>
								</div>
							</div>

							<div class="tab-content container">
								<div id="description" class=" tab-pane active">
									<div class="row">
										<div class="col-xs-12 col-sm-3 col-md-3 center-block hidden-xs">
											<div class=" visible-sm"><div class="text_not_right"></div></div>
											<?php echo $eventimage;?>
										</div>
										<div class="col-xs-12 col-sm-9 col-md-9 text-justify">
											<div class="visible-xs"><div class="text_not_right"></div></div>
											<div class=" visible-sm"><div class="text_not_right"></div></div>
											<?php 
											if ($eventdescbool) {
											 	?><p><?php echo str_replace('href="/', ' target="_blank" href="http://www.zaragoza.es/', $eventdesc); ?></p>
											 <?php
											}else{
											?>

											<p><?php echo $web_actoidjson['title']; ?></p>
											<?php
											} 
											?> <p> <?php
											if ($eventhorariobool) {?>
												<u>Hora</u>:  <?php echo " " . str_replace("</div>", "", str_replace("<div>", "", $eventhorario)); ?><br>
											<?php 
											}else{
												if ($eventhorainiciobool) {?>
													<u>Hora Inicio</u>:  <?php echo " " . str_replace("</div>", "", str_replace("<div>", "", $eventhorainicio)); ?><br>
												<?php 
												}
												if ($eventhorafinalbool) {?>
													<u>Hora Fin</u>:  <?php echo " " . str_replace("</div>", "", str_replace("<div>", "", $eventhorafinal)); ?><br>
												<?php 
												}
											}
											if ($eventhorinscbool) {?>
												<u>Horario inscripción</u>:  <?php echo " " . $eventhorinsc['horarioInscrip'] ?><br>
											<?php 
											}
											if ($eventfechainiciobool) {
												if($eventfechafinalbool){
													if($eventfechainicio != $eventfechafinal){
													?>
														<u>Fecha Inicio</u>:  <?php $fechaini = explode("-", substr($eventfechainicio, 0, 10)); echo " " . $fechaini[2] . '/' . $fechaini[1] . '/' . $fechaini[0]; ?><br>
													<?php 
													}else{
														?>
															<u>Fecha</u>:  <?php $fechaini = explode("-", substr($eventfechainicio, 0, 10)); echo " " . $fechaini[2] . '/' . $fechaini[1] . '/' . $fechaini[0]; ?><br>
														<?php 
													}
												}else{
													?>
													<u>Fecha</u>:  <?php $fechaini = explode("-", substr($eventfechainicio, 0, 10)); echo " " . $fechaini[2] . '/' . $fechaini[1] . '/' . $fechaini[0]; ?><br>
													<?php 
												}
											}
											if ($eventfechafinalbool) {
												if($eventfechainiciobool){
													if($eventfechainicio != $eventfechafinal){
													?>
													<u>Fecha Fin</u>: <?php $fechafin = explode("-", substr($eventfechafinal, 0, 10)); echo " " . $fechafin[2] . '/' . $fechafin[1] . '/' . $fechafin[0]; ?><br>
												<?php 
													}
												}else{
													?>
													<u>Fecha Fin</u>: <?php $fechafin = explode("-", substr($eventfechafinal, 0, 10)); echo " " . $fechafin[2] . '/' . $fechafin[1] . '/' . $fechafin[0]; ?><br>
												<?php
												}

											}
											if ($eventprecioentradabool) {?>
												<u>Precio de Entrada</u>:  <?php echo " " . str_replace("</div>", "", str_replace("<div>", "", $eventprecioentrada)); ?><br>
											<?php 
											}
											if ($evententradalibre) {?>
												<u>Tipo de Entrada</u>:  Libre<br>
											<?php 
											}
											if ($eventtemabool) {?>
												<u>Tema</u>:  <?php echo " " . $eventtema ?><br>
											<?php 
											}
											if ($eventpoblacionbool) {?>
												<u>Dirigido a</u>:  <?php echo " " . $eventpoblacion ?><br>
											<?php 
											}
											if ($eventanexodocbool) {?>
												<a href=<?php echo '"' . $eventanexodoc . '"'; ?> target="_blank"><u>Anexo</u></a><br>
											<?php 
											}
											?>
											</p>
										</div>
									</div>
								</div>
								<div id="subEvent" class=" tab-pane">
									<div class="row">
										<div class="col-xs-12 col-sm-3 col-md-3 hidden-xs">
											<div class="visible-sm"><div class="text_not_right"></div></div>
											<?php echo $eventimage; ?>
										</div>
										<?php if ($eventgeometrybool){?>
										
										<div class="visible-xs">
											<div class="col-xs-12 col-sm-9 col-md-6 text-justify">
												<div class=" visible-sm"><div class="text_not_right"></div></div>
												<?php 
												$maplong = $eventgeometry['coordinates'][1];
												$maplati = $eventgeometry['coordinates'][0];
												$mapsrc = '"https://www.google.com/maps/embed/v1/place?key=AIzaSyBD9VvUcXQFvRS-m2iJOakjP3UoIJRaktQ&q=' . $maplong . ',' . $maplati . '&zoom=17"';
												?>
												<div class="embed-responsive embed-responsive-16by9">
													<iframe  class="embed-responsive-item" src=<?php echo $mapsrc; ?>></iframe>
												</div>
											</div>
											<br>
										</div>
											<div class="visible-xs">
												<div class="col-xs-12 text_not_right"></div>
											</div>
											<?php echo '<div class="col-xs-12 col-sm-3 col-md-3 ">';
										}else{
											echo '<div class="col-xs-12 col-sm-9 col-md-9">';
										} ?>
											<div class=" visible-sm"><div class="text_not_right"></div></div>
											<address class="text-justify">
												<?php 
												if ($eventlugartitlebool) {
												 	echo "<strong>" . $eventlugartitle . "</strong><br>";
												}
												if ($eventlugardireccionbool) {
												 	echo $eventlugardireccion . "<br>";
												}
												if ($eventlugarcpbool) {
												 	echo $eventlugarcp . "<br>";
												}

												if ($eventlugarlocalidadbool) {
													if ($eventlugarprovinciabool) {
														echo $eventlugarlocalidad . " - " . $eventlugarprovincia . "<br>";
													}else{
														echo $eventlugarlocalidad . "<br>";
													}
												}elseif ($eventlugarprovinciabool) {
													if ($eventlugarlocalidadbool) {
														echo $eventlugarlocalidad . " - " . $eventlugarprovincia . "<br>";
													}else{
														echo $eventlugarprovincia . "<br>";
													}
												}

												if ($eventlugarpaisbool) {
												 	echo $eventlugarpais . "<br>";
												}
												if ($eventlugarmailbool) {
												 	echo '<a href="mailto:#">' . $eventlugarmail . '</a>' . "<br>";
												}
												if ($eventlugarautobusesbool) {
												 	echo "Autobuses: " . $eventlugarautobuses . "<br>";
												}
												if ($eventlugartelefonobool) {
												 	echo "Tfno: " . $eventlugartelefono . "<br>";
												}
												if ($eventlugarfaxbool) {
												 	echo "Fax: " . $eventlugarfax . "<br>";
												}
												
												if ($eventlugaraccesibilidadbool) {
												 	echo "<br>" . str_replace('href="/', ' target="_blank" href="http://www.zaragoza.es/', str_replace('src="/cont', 'src="http://www.zaragoza.es/cont', $eventlugaraccesibilidad)) . "<br>";
												}

												?>
											</address>
										</div>
										<?php if ($eventgeometrybool){ ?>
										<div class="hidden-xs">
											<div class="visible-sm"><div class="text_not_right"></div></div>
											<div class="col-xs-12 col-sm-9 col-md-6 text-justify">
												<?php 
												$maplong = $eventgeometry['coordinates'][1];
												$maplati = $eventgeometry['coordinates'][0];
												$mapsrc = '"https://www.google.com/maps/embed/v1/place?key=AIzaSyBD9VvUcXQFvRS-m2iJOakjP3UoIJRaktQ&q=' . $maplong . ',' . $maplati . '&zoom=17"';
												?>
												<div class="embed-responsive embed-responsive-16by9">
													<iframe  class="embed-responsive-item" src=<?php echo $mapsrc; ?>></iframe>
												</div>
											</div>
										</div>
										<?php } ?>
									</div>
								</div>
								
								<div id="entidad" class=" tab-pane">
									<div class="row ">
										<div class="col-xs-12 col-sm-3 col-md-3 center-block hidden-xs">
											<div class=" visible-sm"><div class="text_not_right"></div></div>
											<?php echo $eventimage; ?>
										</div>
										<div class="col-xs-12 col-sm-9 col-md-9 text-justify">
											<div class="visible-xs"><div class="text_not_right"></div></div>
											<div class=" visible-sm"><div class="text_not_right"></div></div>
											<?php 
											if ($evententidadtitlebool) {
												echo "<strong>" . $evententidadtitle . "</strong><br>";
											}
											if ($evententidaddireccionbool) {
												echo $evententidaddireccion . "<br>";
											}
											if ($evententidadcpbool) {
												echo $evententidadcp . "<br>";
											}
											if ($evententidadlocalidadbool) {
												if ($evententidadprovinciabool) {
													echo $evententidadlocalidad . " - " . $evententidadprovincia . "<br>";
												}else{
													echo $evententidadlocalidad . "<br>";
												}
											}elseif ($evententidadprovinciabool) {
												if ($evententidadlocalidadbool) {
													echo $evententidadlocalidad . " - " . $evententidadprovincia . "<br>";
												}else{
													echo $evententidadprovincia . "<br>";
												}
											}
											if ($evententidadpaisbool) {
												echo $evententidadpais . "<br>";
											}
											if ($evententidadhorariobool) {
												echo "Horario" . $evententidadhorario . "<br>";
											}
											if ($evententidadtelefonobool) {
												echo "Tfno: " . $evententidadtelefono . "<br>";
											}
											if ($evententidadtelefono2bool) {
												echo "Tfno: " . $evententidadtelefono2 . "<br>";
											}
											if ($evententidadfaxbool) {
												echo "Fax: " . $evententidadfax . "<br>";
											}
											if ($evententidademailbool) {
												echo '<a href="mailto:#">' . $evententidademail . "</a><br>";
											}
											if ($evententidadwebbool) {
												echo '<a href="' . 'http://' .  $evententidadweb . '" target="_blank">' . $evententidadweb . "</a><br>";
											}
											?>
										</div>
									</div>
								</div>
								<div id="compartir" class=" tab-pane">
									<div class="row ">
										<div class="col-xs-12 col-sm-3 col-md-3 left-block hidden-xs ">
											<div class=" visible-sm"><div class="text_not_right"></div></div>
											<?php echo $eventimage; ?>
										</div>
										<div class="col-xs-12 col-sm-9 col-md-9 text-justify ">
											<div class=" visible-sm visible-xs"><div class="text_not_right"></div></div>
											<div class="row">
											<?php 
											$actuallink = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
											$actualtitle = $web_actoidjson['title'];
											?>
											<div class=" col-xs-6 col-sm-4 col-md-4 ">
												<!--https://twitter.com/intent/tweet?url=http%3A%2F%2Fpilares.info&text=Pilares%202015%20%7C%20Nervo&hashtags=Pilares2015-->
												<a title="Comparte en Twitter" href=<?php echo '"https://twitter.com/intent/tweet?url=http%3A%2F%2Fpilares.info%2Fevento%2F?id=' . $web_actoidjson['id'] . '&text=Pilares%202015%20%7C%20' . $actualtitle . '&hashtags=Pilares2015"'; ?> target="_blank">
													<img class="img-responsive" src="../img/twitter.jpg">
												</a>
											</div>
											<div class=" col-xs-6 col-sm-4 col-md-4">
												<!--https://www.facebook.com/sharer/sharer.php?u=http%3A//pilares.info/evento/?id=110687-->
												<a title="Comparte en Facebook" href=<?php echo '"https://www.facebook.com/sharer/sharer.php?u=http%3A//pilares.info/evento/?id=' . $web_actoidjson['id'] . '"'; ?> target="_blank">
													<img class="img-responsive" src="../img/facebook.jpg">
												</a>
											</div>
											<div class=" col-xs-12 visible-xs"><br></div>
											<div class=" col-xs-6 col-sm-4 col-md-4">
												<!--https://plus.google.com/share?url=http%3A//pilares.info/evento/?id=110687-->
												<a title="Comparte en Google+" href=<?php echo '"https://plus.google.com/share?url=http%3A//pilares.info/evento/?id=' . $web_actoidjson['id'] . '"'; ?> target="_blank">
													<img class="img-responsive" src="../img/googleplus.jpg">
												</a>
											</div>
											<div class=" col-xs-6 col-sm-4 col-md-4">
												<div class="visible-xs">
													<a href= <?php echo '"'.'whatsapp://send?text=Pilares 2015 | ' . $web_actoidjson['title'] . ' - ' . $actuallink . '"'; ?>><img alt="Compartir en WhatsApp" class="img-responsive" src="../img/whatsapp.jpg"></a>
												</div>
											</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					  	
					   	<?php 

					   	//<a href="/ciudad/fiestaspilar/intro.htm">
					}
					
					//http://www.zaragoza.es/api/recurso/cultura-ocio/evento-zaragoza/127876.json
				}
				
				?>
		</div>
	<footer class="footer footer_not_right">
		<div class="container">
			<div class="row hidden-xs">
				<div class="hidden-xs col-sm-6 col-md-6 text_not_right">
					<p class="text-muted text-left">
					<?php if ($eventupdatebool){ ?>
						<?php echo "Última actualización datos: " .  explode("-", substr($eventupdate, 0, 10))[2] . '/' . explode("-", substr($eventupdate, 0, 10))[1] . '/' . explode("-", substr($eventupdate, 0, 10))[0];?>
					<?php }?>
					</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 text_not_right">
					<p class="text-muted text-right">Origen de los datos: Ayuntamiento de Zaragoza</p>
				</div>
			</div>
			<div class="row visible-xs text">
				<div class="col-xs-12 first_row_space">
					<p class="text-muted text-center">Origen de los datos: Ayuntamiento de Zaragoza</p><p class="text-muted text-center footer_text_space"><?php if ($eventupdatebool){ ?>
						<?php echo "Última actualización datos: " .  explode("-", substr($eventupdate, 0, 10))[2] . '/' . explode("-", substr($eventupdate, 0, 10))[1] . '/' . explode("-", substr($eventupdate, 0, 10))[0];?>
					<?php }?></p>
				</div>
			</div>
		</div>
	</footer>

		<script src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=10640487; 
var sc_invisible=1; 
var sc_security="102d0788"; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="shopify stats"
href="http://statcounter.com/shopify/" target="_blank"><img
class="statcounter"
src="http://c.statcounter.com/10640487/0/102d0788/1/"
alt="shopify stats"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->
</body>
</html>