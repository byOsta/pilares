<html>
<head>
	<meta charset="UTF-8">
	<title>Fiestas del Pilar 2015</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="icon" type="image/png" href="img/favicon.png">
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><img style="max-width:90px; margin-top: -37px;" alt="Brand" src="img/favicon.png"></a>
				<a class="navbar-brand" href="#">Pilares 2015</a>
			</div>

			<!--<a class="navbar-brand" href="#">Pilares 2015</a>-->
		
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				
				<ul class="nav navbar-nav text-center">
					<li><a href="buscador?srcname=">Eventos</a></li>
				</ul>

				<ul class="nav navbar-nav text-center">
					<li><a href= <?php echo '"buscador?fechainicio=' . str_replace("/", "%2F", date("d/m/Y")) . '"' ?>>Hoy</a></li>
					<!--02%2F10%2F2015-->
				</ul>

				<ul class="nav navbar-nav text-center">
					<?php 
					$datemanana;
					if(date("d") > date("t")){
						$datedia = 1;
						$datemes = date("m") + 1;
						$datemanana = str_replace("/", "%2F", $datedia . "/" . $datemes . date("/Y"));
					}else{
						$datedia = date("d") + 1;
						$datemanana = str_replace("/", "%2F", $datedia . "/" . date("m/Y"));
					}

					 ?>
					<li><a href= <?php echo '"buscador?fechainicio=' . $datemanana . '"' ?>>Mañana</a></li>
				</ul>

			
				<form class="nav navbar-form navbar-right visible-xs" role="search" action="buscador">
						<div class="row">
							<div class="form-group">
								<div class="col-xs-1"></div>
								<div class="col-xs-8">
									<input type="text" class="form-control" name="srcname" placeholder="Búsqueda Rápida">
								</div>
								<div class="col-xs-3">
									<button type="submit" class="btn btn-default">Buscar</button>
								</div>
							</div>
						</div>
				</form>

				<form class="nav navbar-form navbar-right hidden-xs hidden-sm" role="search" action="buscador">
					<div class="form-group">
						<input type="text" class="form-control hidden-xs" name="srcname" placeholder="Búsqueda Rápida">
						<button type="submit" class="btn btn-default hidden-xs">Buscar</button>
					</div>
				</form>
				
				<form class="nav navbar-form navbar-right visible-sm" role="search" action="buscador">
					<div class="form-group">
						<input type="text" style="width: 150px;" class="form-control hidden-xs" name="srcname" placeholder="Búsqueda Rápida">
						<button type="submit" class="btn btn-default hidden-xs">Buscar</button>
					</div>
				</form>
			</div>
		</div>
	</nav>

	<div class="container hidden-sm">
		<div class="row first_container_not_right">
			<a href="buscador/?tema=16">
				<div class="col-xs-6 col-md-3 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/16.jpg">
					<h3 class="typetxt">Música</h3>
				</div>
			</a>
			<a href="buscador/?tema=22">
				<div class="col-xs-6 col-md-3 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/22.jpg">
					<h3 class="typetxt">Fiestas</h3>
				</div>
			</a>
			<div class="clearfix visible-xs-block"></div>
			<a href="buscador/?tema=23">
				<div class="col-xs-6 col-md-3 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/23.jpg">
					<h3 class="typetxt">Ferias, Muestras y Salones</h3>
				</div>
			</a>
			<a href="buscador/?tema=79">
				<div class=" col-xs-6 col-md-3 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/79.jpg">
					<h3 class="typetxt">Actos Religiosos</h3>
				</div>
			</a>
		</div>
		<div class="row">
			<a href="buscador/?tema=19">
				<div class=" col-xs-6 col-md-3 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/19.jpg">
					<h3 class="typetxt">Exposiciones y museos</h3>
				</div>
			</a>
			<a href="buscador/?tema=25">
				<div class=" col-xs-6 col-md-3 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/25.jpg">
					<h3 class="typetxt">Deportes</h3>
				</div>
			</a>
			<div class="clearfix visible-xs-block"></div>
			<a href="buscador/?tema=37">
				<div class=" col-xs-6 col-md-3 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/37.jpg">
					<h3 class="typetxt">Bibliotecas</h3>
				</div>
			</a>
			<a href="buscador/?tema=26">
				<div class="col-xs-6 col-md-3 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/17.jpg">
					<h3 class="typetxt">Teatro</h3>
				</div>
			</a>
		</div>
		<div class="row">
			<a href="buscador/?tema=17">
				<div class="col-xs-3 hidden-sm hidden-md"></div>
				<div class="col-xs-6 col-md-12 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/26.jpg">
					<h3 class="typetxt">Otros</h3>
				</div>
				<div class="col-xs-3 hidden-sm hidden-md"></div>
			</a>
		</div>
	</div>	
		
	<div class="container visible-sm">
		<div class="row first_container_not_right">
			<a href="buscador/?tema=79">
				<div class=" col-sm-4 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/16.jpg">
					<h3 class="typetxt">Música</h3>
				</div>
			</a>
			<a href="buscador/?tema=37">
				<div class=" col-sm-4 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/22.jpg">
					<h3 class="typetxt">Fiestas</h3>
				</div>
			</a>
			<a href="buscador/?tema=25">
				<div class=" col-sm-4 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/23.jpg">
					<h3 class="typetxt">Ferias, Muestras y Salones</h3>
				</div>
			</a>
		</div>
		<div class="row">
			<a href="buscador/?tema=19">
				<div class=" col-sm-4 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/79.jpg">
					<h3 class="typetxt">Actos Religiosos</h3>
				</div>
			</a>
			<a href="buscador/?tema=23">
				<div class="col-sm-4 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/19.jpg">
					<h3 class="typetxt">Exposiciones y museos</h3>
				</div>
			</a>
			<a href="buscador/?tema=22">
				<div class="col-sm-4 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/25.jpg">
					<h3 class="typetxt">Deportes</h3>
				</div>
			</a>
		</div>
		<div class="row">
			<a href="buscador/?tema=16">
				<div class="col-sm-4 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/37.jpg">
					<h3 class="typetxt">Bibliotecas</h3>
				</div>
			</a>
			<a href="buscador/?tema=26">
				<div class="col-sm-4 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/17.jpg">
					<h3 class="typetxt">Teatro</h3>
				</div>
			</a>
			<a href="buscador/?tema=17">
				<div class="col-sm-4 text-center">
					<img class="img-responsive typeimg center-block" src="img/temas/26.jpg">
					<h3 class="typetxt">Otros</h3>
				</div>
			</a>
		</div>
	</div>

	<footer class="footer footer_not_right">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-6 text_not_right hidden-xs">
					 	<?php 
						$web_nactos = 'http://www.zaragoza.es/api/recurso/cultura-ocio/evento-zaragoza.json?fl=id&q=programa==Fiestas%20del%20Pilar&rows=1';
						$web_nactoscontent = file_get_contents($web_nactos);
						$web_nactosjson = json_decode($web_nactoscontent, true); 
						?>

						<p class="hidden-xs text-muted text-left">Hay <strong><?php echo $web_nactosjson['totalCount']; ?></strong> eventos para fiestas del Pilar</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 text_not_right">
					<p class="text-muted text-right">Origen de los datos: Ayuntamiento de Zaragoza</p>
				</div>
			</div>
		</div>
	</footer>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=10640487; 
var sc_invisible=1; 
var sc_security="102d0788"; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="shopify stats"
href="http://statcounter.com/shopify/" target="_blank"><img
class="statcounter"
src="http://c.statcounter.com/10640487/0/102d0788/1/"
alt="shopify stats"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->
</body>

</html>